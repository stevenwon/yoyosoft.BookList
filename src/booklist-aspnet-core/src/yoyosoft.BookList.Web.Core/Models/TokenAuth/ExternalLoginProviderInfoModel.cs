using Abp.AutoMapper;
using yoyosoft.BookList.Authentication.External;

namespace yoyosoft.BookList.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
