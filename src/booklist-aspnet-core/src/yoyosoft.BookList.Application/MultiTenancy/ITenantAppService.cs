using Abp.Application.Services;
using Abp.Application.Services.Dto;
using yoyosoft.BookList.MultiTenancy.Dto;

namespace yoyosoft.BookList.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
