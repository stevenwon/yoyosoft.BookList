using System.Threading.Tasks;
using Abp.Application.Services;
using yoyosoft.BookList.Authorization.Accounts.Dto;

namespace yoyosoft.BookList.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
