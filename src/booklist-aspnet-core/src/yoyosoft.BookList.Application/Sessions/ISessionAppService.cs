using System.Threading.Tasks;
using Abp.Application.Services;
using yoyosoft.BookList.Sessions.Dto;

namespace yoyosoft.BookList.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
