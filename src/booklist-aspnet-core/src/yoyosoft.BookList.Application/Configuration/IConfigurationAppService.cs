using System.Threading.Tasks;
using yoyosoft.BookList.Configuration.Dto;

namespace yoyosoft.BookList.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
