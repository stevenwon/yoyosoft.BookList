using System.ComponentModel.DataAnnotations;

namespace yoyosoft.BookList.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}