using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using yoyosoft.BookList.Roles.Dto;
using yoyosoft.BookList.Users.Dto;

namespace yoyosoft.BookList.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
