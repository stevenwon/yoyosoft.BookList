using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using yoyosoft.BookList.Authorization.Roles;
using yoyosoft.BookList.Authorization.Users;
using yoyosoft.BookList.MultiTenancy;

namespace yoyosoft.BookList.EntityFrameworkCore
{
    public class BookListDbContext : AbpZeroDbContext<Tenant, Role, User, BookListDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public BookListDbContext(DbContextOptions<BookListDbContext> options)
            : base(options)
        {
        }
    }
}
