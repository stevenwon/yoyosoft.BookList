using Abp.Authorization;
using yoyosoft.BookList.Authorization.Roles;
using yoyosoft.BookList.Authorization.Users;

namespace yoyosoft.BookList.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
