﻿using Abp.Domain.Entities.Auditing;

namespace yoyosoft.BookList.BookListManagement.Books
{
    /// <summary>
    /// 书籍
    /// </summary>
    public class Book:CreationAuditedEntity<long>
    {
        /// <summary>
        /// 书名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }

        /// <summary>
        /// 价格链接
        /// </summary>
        public string PriceUrl { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string ImgUrl { get; set; }
    }
}
