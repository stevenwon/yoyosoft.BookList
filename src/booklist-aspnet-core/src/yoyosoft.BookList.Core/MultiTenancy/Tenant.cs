using Abp.MultiTenancy;
using yoyosoft.BookList.Authorization.Users;

namespace yoyosoft.BookList.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
