using Abp.Application.Features;
using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using yoyosoft.BookList.Authorization.Users;
using yoyosoft.BookList.Editions;

namespace yoyosoft.BookList.MultiTenancy
{
    public class TenantManager : AbpTenantManager<Tenant, User>
    {
        public TenantManager(
            IRepository<Tenant> tenantRepository, 
            IRepository<TenantFeatureSetting, long> tenantFeatureRepository, 
            EditionManager editionManager,
            IAbpZeroFeatureValueStore featureValueStore) 
            : base(
                tenantRepository, 
                tenantFeatureRepository, 
                editionManager,
                featureValueStore)
        {
        }
    }
}
